import requests
import unittest
unittest.TestLoader.sortTestMethodsUsing = None
from pymongo import MongoClient


BASE_URL = "127.0.0.1"
PORT = "5000"

"""
Usage, start server with clean DB.
"""
class FunctionalUnitTest(unittest.TestCase):

    def _get_mongo_collection(self):
        self.client = MongoClient('localhost', 27017)
        self.col = self.client.flask_db.checkr
        return self.col

    def _get_user_id(self):
        # Get user by these attributes
        data = {
            "firstName": "Charles",
            "lastName": "Smiths"
        }
        res = requests.get(f"http://{BASE_URL}:{PORT}/get_user_by_attributes", json=data)
        id = res.json()["data"][0]["_id"]
        return res, id

    def test_all(self):
        self._test_version()
        self._test_bad_data()
        self._test_insert()
        self._test_query()
        self._test_bulk_insert()
        self._test_update()
        self._test_delete()


    def _test_version(self):
        res = requests.get(f"http://{BASE_URL}:{PORT}/get_version")
        self.assertEqual(res.json()["data"], '1.0.0')


    def _test_bad_data(self):
        # Bad
        data = {
            "id": "123",
            "name" : "Charles Hennessey"
        }
        res = requests.post(f"http://{BASE_URL}:{PORT}/create_user", json=data)
        print(res.json()["data"])
        self.assertEqual(res.json()["data"], "Insert failed schema validation")
    
    def _test_insert(self):
        data = {
            "firstName": "Charles",
            "lastName": "Hennessey",
            "streetAddress": "401 Conestoga Way",
            "city": "Norristown",
            "state": "PA",
            "zipCode": "19403"
        }
        res = requests.post(f"http://{BASE_URL}:{PORT}/create_user", json=data)
        self.assertEqual(res.json()["data"], "User created")

    def _test_query(self):
        # Get user by these attributes
        res, _ = self._get_user_id()
        self.assertEqual(len(res.json()["data"]), 1)

    def _test_bulk_insert(self):
        data = [
            {
                #"_id": "123",
                "firstName": "John",
                "lastName": "Doe",
                "streetAddress": "402 Conestoga Way",
                "city": "Borristown",
                "state": "PA",
                "zipCode": "19403"
            },
            {
                #"_id": "123",
                "firstName": "Joey",
                "lastName": "Trails",
                "streetAddress": "401 Conestoga Way",
                "city": "Haoppy",
                "state": "PA",
                "zipCode": "19403"
            },
            {
                #"_id": "456",
                "firstName": "Maddy",
                "lastName": "Hennessey",
                "streetAddress": "401 Conestoga Way",
                "city": "Norristown",
                "state": "PA",
                "zipCode": "19403"
            },
        ]
        res = requests.post(f"http://{BASE_URL}:{PORT}/create_many_users", json=data)  # Two should insert
        self.assertEqual(res.json()["data"], "Users created")


    def _test_update(self):
        _, id = self._get_user_id()
        data =  {
            "query" : {
                "_id": id,
                "firstName": "Charles",
                "lastName": "Hennessey",
                "streetAddress": "401 Conestoga Way",
                "city": "Norristown",
                "state": "PA",
                "zipCode": "19403"
            },
            "updates": {
                "lastName" : "Smiths"
            }
            
        }

        res = requests.post(f"http://{BASE_URL}:{PORT}/update_user_by_id", json=data)  # All
        self.assertEqual(res.json()["data"], f"User {id} updated")

    def _test_delete(self):
        _, id = self._get_user_id()
        data =  {
            "_id": id
        }
        res = requests.post(f"http://{BASE_URL}:{PORT}/delete_by_id", json=data)  # All
        self.assertEqual(res.json()["data"], f"User {id} deleted")

if __name__ == "__main__":
    unittest.main()