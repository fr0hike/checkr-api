def get_format():
    return {
    "collMod": "checkr",
    "validationLevel": "moderate",
    "validator": {
        "$jsonSchema": get_validator()
    }
}



def get_validator(): 
    return {
            "bsonType": "object",
            "description": "Document describing person",
            "required": ["firstName", "lastName", "streetAddress", "city", "state", "zipCode"],
            "properties": {
                "firstName": {
                    "bsonType": "string",
                    "description": "First name"
                },
                "lastName": {
                    "bsonType": "string",
                    "description": "Last name"
                },
                "streetAddress": {
                    "bsonType": "string",
                    "description": "Person's Address"
                },
                "city": {
                    "bsonType": "string",
                    "description": "Person's city in which they reside"
                },
                "state": {
                    "bsonType": "string",
                    "description": "Person's state in which they reside"
                },
                "zipCode": {
                    "bsonType": "string",
                    "description": "Person's zipcode in which they reside"
                }
            },
        }
