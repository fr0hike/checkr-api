# Requirements

- Must be able to read and parse a CSV on server start, load into MongoDB

- Must be able to perform CRUD operations via rest (client.py)

## Hidden requrements
- Must have model class for generic data
- Must have controller class for Database interactions
- Must adhere to schema