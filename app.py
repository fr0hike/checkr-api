from flask import Flask, request, make_response
from pymongo.errors import WriteError, BulkWriteError, InvalidOperation
from bson.errors import InvalidId
from .response_model import ResponseModel
from http import HTTPStatus
import json
_version = "1.0.0"

from .controllers.database_controller import Database

db = Database()

app = Flask(__name__)

@app.route("/get_version", methods=["GET"])
def get_version():
    return make_response(ResponseModel(_version, HTTPStatus.OK).response())

@app.route("/create_user", methods=["POST"])
def create_user():
    try:
        if request.data:
            data = json.loads(request.data)
            db.insert(data)
            return make_response(ResponseModel("User created", HTTPStatus.CREATED).response())
    except WriteError as e:
        return make_response(ResponseModel("Insert failed schema validation", HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)
    except InvalidOperation as e:
        return make_response(ResponseModel("Data already exists", HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)
    except InvalidId as e:
        return make_response(ResponseModel("Incorrect ID format, please refer to ObjectId docs", HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)
    except Exception as e:
        return make_response(ResponseModel(e, HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)


@app.route("/create_many_users", methods=["POST"])
def create_many_users():
    try:
        if request.data:
            data = json.loads(request.data)
            db.insert_many(data)
            return make_response(ResponseModel("Users created", HTTPStatus.CREATED).response())
    except WriteError as e:
        return make_response(ResponseModel("Insert failed schema validation", HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)
    except InvalidOperation as e:
        return make_response(ResponseModel("Data already exists", HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)
    except BulkWriteError as e:
        return make_response(ResponseModel("Two identical ids were attempted to be inserted", HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)

@app.route("/get_user_by_attributes", methods=["get"])
def get_user_by_attributes():
    try:
        if request.data:
            data = json.loads(request.data)
            results = db.find(data)
            return make_response(ResponseModel(results, HTTPStatus.OK).response())
    except Exception as e:
        return make_response(ResponseModel(e, HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)


@app.route("/update_user_by_id", methods=["post"])
def update_user_by_id():
    try:
        if request.data:
            data = json.loads(request.data)
            _id = { "_id" : data["query"]["_id"] }
            results = db.find_one(_id)
            if results:
                db.update_one(_id, data["updates"])
                _id = _id["_id"]
                return make_response(ResponseModel(f"User {_id} updated", HTTPStatus.OK).response())
            else:
                return make_response(ResponseModel(f"User {_id} doesn't exist", HTTPStatus.NO_CONTENT).response())
    except Exception as e:
        return make_response(ResponseModel(e, HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)

@app.route("/delete_by_id", methods=["post"])
def delete_by_id():
    try:
        if request.data:
            data = json.loads(request.data)
            _id = { "_id" : data["_id"] }
            res = db.delete_one(_id)
            _id = data["_id"]
            if res.deleted_count == 1:
                message = f"User {_id} deleted"
            else:
                message = f"User {_id} didn't exist"
            return make_response(ResponseModel(message, HTTPStatus.OK).response())

    except Exception as e:
        return make_response(ResponseModel(e, HTTPStatus.INTERNAL_SERVER_ERROR).response(), HTTPStatus.INTERNAL_SERVER_ERROR)