from pymongo import MongoClient

from pymongo.errors import CollectionInvalid, InvalidOperation
from bson.objectid import ObjectId
from bson.errors import InvalidId
from ..model.schema import get_format, get_validator

# Relevant: https://pymongo.readthedocs.io/en/stable/tutorial.html

class Database(object):
    DB = None
    COLLECTION = None

    @staticmethod
    def init():
        client = MongoClient('localhost', 27017)
        Database.DB = client.flask_db
        try:
            Database.COLLECTION = Database.DB.create_collection(name="checkr", 
                validator={
                    "$jsonSchema" : get_validator()
                }
            )
            Database._load_csv_data_from_disk()
        except CollectionInvalid as e:
            print("Collection already exists")
            Database.COLLECTION = Database.DB.checkr

    @staticmethod
    def insert(data):
        result = Database.find_one(data)
        if result is None:
            return Database.COLLECTION.insert_one(data)
        raise InvalidOperation

    @staticmethod
    def insert_many(data):
        return Database.COLLECTION.insert_many(data, ordered=False)
        

    @staticmethod
    def find(query):
        query = Database._parse_id_query(query)
        result = Database.COLLECTION.find(query)
        output = []
        for doc in result:
            doc["_id"] = str(doc["_id"]) # cant JSONify the ObjectId
            output.append(doc)
        return output
    
    @staticmethod
    def find_one(query):
        query = Database._parse_id_query(query)
        return Database.COLLECTION.find_one(query)
    
    @staticmethod
    def update_one(_id, data):
        _id = Database._parse_id_query(_id)
        # del query["_id"]
        filter = _id
        updated_values = {
            "$set": data
        }
        return Database.COLLECTION.update_one(filter, updated_values)

    @staticmethod
    def delete_one(_id):
        _id = Database._parse_id_query(_id)
        return Database.COLLECTION.delete_one(_id)

    @staticmethod
    def get_all():
        curser = Database.COLLECTION.find({})
        results = []
        for doc in curser:
            results.append(doc)
        return results

    def _parse_id_query(query):
        try:
            if "_id" in query:
                query["_id"] = ObjectId(query["_id"])
            return query
        except InvalidId as e:
            raise e