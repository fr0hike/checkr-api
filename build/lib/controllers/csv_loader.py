import csv
import repackage
repackage.up()
from .database_controller import Database

class CSVLoader(Database):
    def load(fn="/home/chuck/Desktop/checkr-api/client/data.csv") -> None:
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            # Skip headers
            next(reader)
            for row in reader:
                first_name, last_name, street_address, city, state, zip_code = row
                Database.insert({
                    "firstName" : first_name,
                    "lastName" : last_name,
                    "streetAddress" : street_address,
                    "city" : city,
                    "state" : state,
                    "zipCode" : zip_code,
                })
if __name__ == "__main__":
    CSVLoader.load()
