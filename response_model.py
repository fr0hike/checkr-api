import json

class ResponseModel:
    def __init__(self, data: str, http_status) -> None:
        self.output = {
            "data" : data,
            "http_status": http_status
        }

    def response(self):
        return self.output
