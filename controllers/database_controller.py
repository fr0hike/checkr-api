from pymongo import MongoClient

from pymongo.errors import CollectionInvalid, InvalidOperation
from bson.objectid import ObjectId
from bson.errors import InvalidId
from ..model.schema import get_format, get_validator

# Relevant: https://pymongo.readthedocs.io/en/stable/tutorial.html

class Database(object):

    def __init__(self):
        client = MongoClient('localhost', 27017)
        self.db = client.flask_db
        try:
            self.collection = self.db.create_collection(name="checkr", 
                validator={
                    "$jsonSchema" : get_validator()
                }
            )
        except CollectionInvalid as e:
            print(self, "Collection already exists")
            self.collection = self.db.checkr


    def insert(self, data):
        result = self.find_one(data)
        if result is None:
            return self.collection.insert_one(data)
        raise InvalidOperation

    def insert_many(self, data):
        return self.collection.insert_many(data, ordered=False)
        

    def find(self, query):
        query = self._parse_id_query(query)
        result = self.collection.find(query)
        output = []
        for doc in result:
            doc["_id"] = str(doc["_id"]) # cant JSONify the ObjectId
            output.append(doc)
        return output
    
    def find_one(self, query):
        query = self._parse_id_query(query)
        return self.collection.find_one(query)
    
    def update_one(self, _id, data):
        _id = self._parse_id_query(_id)
        filter = _id
        updated_values = {
            "$set": data
        }
        return self.collection.update_one(filter, updated_values)

    def delete_one(self, _id):
        _id = self._parse_id_query(_id)
        return self.collection.delete_one(_id)

    def get_all(self):
        curser = self.collection.find({})
        results = []
        for doc in curser:
            results.append(doc)
        return results

    def _parse_id_query(self, query):
        try:
            if "_id" in query:
                query["_id"] = ObjectId(query["_id"])
            return query
        except InvalidId as e:
            raise e