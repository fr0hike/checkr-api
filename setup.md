# Quick doc on how I setup this.

## Steps
- Install `pip`
- Install `flask`
- Install mongo on linux

    ```
    https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/
    ```
- Running mongo

```
sudo systemctl start mongod

```