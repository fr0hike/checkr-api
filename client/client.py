import requests
BASE_URL = "127.0.0.1"
PORT = "5000"

res = requests.get(f"http://{BASE_URL}:{PORT}/get_version")

# Bad
data = {
    "id": "123",
    "name" : "Charles Hennessey"
}
res = requests.post(f"http://{BASE_URL}:{PORT}/create_user", json=data)
res.json()

# Bad ID, Good without ID
data = {
    #"_id": "123",
    "firstName": "Charles",
    "lastName": "Hennessey",
    "streetAddress": "401 Conestoga Way",
    "city": "Norristown",
    "state": "PA",
    "zipCode": "19403"
}
res = requests.post(f"http://{BASE_URL}:{PORT}/create_user", json=data)
res.json()


# Good
data = {
    "firstName": "Matthew",
    "lastName": "Test",
    "streetAddress": "Yer",
    "city": "Happy",
    "state": "PA",
    "zipCode": "19403"
}
res = requests.post(f"http://{BASE_URL}:{PORT}/create_user", json=data)
res.json()

# No first name, will fail, if following prior will fail bc similar data exists
data = {
    "lastName": "Hennessey",
    "streetAddress": "401 Conestoga Way",
    "city": "Norristown",
    "state": "PA",
    "zipCode": "19403"
}
res = requests.post(f"http://{BASE_URL}:{PORT}/create_user", json=data)
res.json()


from pymongo import MongoClient
client = MongoClient('localhost', 27017)
col = client.flask_db.checkr


# get all
def all():
    res = col.find({})
    for d in res:
        print(d)

# find Matthews 
res = col.find({"firstName": "Matthew"})
for d in res:
    print(d)



# Get user by these attributes
data = {
    "_id": "6260ba69419f668144dd7d64"
}
res = requests.get(f"http://{BASE_URL}:{PORT}/get_user_by_attributes", json=data)
res.json()

# Bulk insert

# Bad if you use Ids, ok if you bulk insert. Duplicates exist tho.
# Need an ID system to prevent this, not certain how interview will go.
data = [
    {
        #"_id": "123",
        "firstName": "Charles",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
    {
        #"_id": "123",
        "firstName": "Charles",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
    {
        #"_id": "456",
        "firstName": "Charles",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
]
res = requests.post(f"http://{BASE_URL}:{PORT}/create_many_users", json=data)  # Two should insert
res.json()



# More good inserts
data = [
    {
        "firstName": "Charles",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
    {
        "firstName": "Patrick",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
    {
        "firstName": "matthew",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
]
res = requests.post(f"http://{BASE_URL}:{PORT}/create_many_users", json=data)  # All
res.json()

# update Charles

data =  {
    "query" : {
        "_id": "6260baaaff31969d1bd3f51c",
        "firstName": "Charles",
        "lastName": "Hennessey",
        "streetAddress": "401 Conestoga Way",
        "city": "Norristown",
        "state": "PA",
        "zipCode": "19403"
    },
    "updates": {
        "lastName" : "Smiths"
    }
    
}

res = requests.post(f"http://{BASE_URL}:{PORT}/update_user_by_id", json=data)  # All
res.json()

#del

data =  {
    "_id": "625e200d86a56e73f2a40429"
}

res = requests.post(f"http://{BASE_URL}:{PORT}/delete_by_id", json=data)  # All
res.json()