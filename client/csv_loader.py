import csv
import requests


class CSVLoader():

    def read_data_from_csv(self, fn="/home/chuck/Desktop/checkr-api/client/data.csv") -> None:
        database_data = []
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            # Skip headers
            next(reader)
            for row in reader:
                first_name, last_name, street_address, city, state, zip_code = row
                database_data.append({
                    "firstName" : first_name,
                    "lastName" : last_name,
                    "streetAddress" : street_address,
                    "city" : city,
                    "state" : state,
                    "zipCode" : zip_code,
                })
        return database_data
if __name__ == "__main__":
    loader = CSVLoader()
    data = loader.read_data_from_csv()

    BASE_URL = "127.0.0.1"
    PORT = "5000"
    res = requests.post(f"http://{BASE_URL}:{PORT}/create_many_users", json=data)
    print(res.json())
