from setuptools import setup, find_packages

setup(
    name="checkr_api",
    packages=find_packages()
)